package ua.com.testmatick;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class GoogleSearchResultsPage extends PageObject {

    private WebElement searchField = driver.findElement(By.id("lst-ib"));
    private List<WebElement> results = driver.findElements(By.xpath("//div[@id='ires']//div[@class='srg']/div"));

    public GoogleSearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public String getTitle() {
        return driver.getTitle();
    }

    public GoogleSearchResultsPage searchForAdd(String massage) {
        searchField.sendKeys(massage);
        searchField.submit();

        return this;
    }

    public GoogleSearchResultsPage searchFor(String massage) {
        searchField.clear();
        searchField.sendKeys(massage);
        searchField.submit();

        return this;
    }

    public List<WebElement> getResults() {
        return results;
    }

    public GoogleSearchPage gotoGoogleSearchPage() {
        driver.get("https://www.google.com");

        return new GoogleSearchPage(driver);
    }
}

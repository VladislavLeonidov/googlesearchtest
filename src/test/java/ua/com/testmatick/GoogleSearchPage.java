package ua.com.testmatick;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class GoogleSearchPage extends PageObject {

    private WebElement searchField = driver.findElement(By.id("lst-ib"));

    public GoogleSearchPage(WebDriver driver) {
        super(driver);
    }

    public GoogleSearchResultsPage searchFor(String massage) {
        searchField.sendKeys(massage);
        searchField.submit();
        return new GoogleSearchResultsPage(driver);
    }
}

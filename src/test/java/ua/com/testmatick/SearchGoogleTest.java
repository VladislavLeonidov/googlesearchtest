package ua.com.testmatick;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class SearchGoogleTest {

    private WebDriver chromeDriver;

    @BeforeTest
    private void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:/browsersDrivers/chromedriver.exe");
        chromeDriver = new ChromeDriver();
        chromeDriver.manage().window().maximize();
        chromeDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        chromeDriver.get("https://www.google.com");
    }

    @Test
    public void searchSeleniumWebDriverTest() {
        String searchMessage = "selenium webdriver";
        List<WebElement> results = new GoogleSearchPage(chromeDriver).
                searchFor(searchMessage).
                getResults();
        Assert.assertTrue(new GoogleSearchResultsPage(chromeDriver).getTitle().startsWith(searchMessage));
        Assert.assertEquals(results.size(), 10);
    }

    @AfterTest
    private void tearDown() {
        if (chromeDriver != null) {
            chromeDriver.quit();
        }
    }
}
